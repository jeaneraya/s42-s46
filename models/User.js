const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	email : { type: String, required: [true, "This field is required"], unique: true },
	password: { type: String, required: [true, "This field is required"] },
	isAdmin: { type: Boolean, default: false },
})


module.exports = mongoose.model("User", userSchema);