const Product = require('../models/Product')
const Order = require('../models/Order')

// ADD CUSTOMER ORDER
module.exports.addUserOrder = (reqBody, userData) => {
    if (userData.isAdmin == false) {
        
        return Product.findById(reqBody.productId).then(products => {
            console.log(products)
            let addUserOrder = new Order ({
            userId: userData.userId,
            products:{
               productId: reqBody.productId,
               quantity: reqBody.quantity, 
            },
            totalAmount: reqBody.quantity * products.price
        })
                if(products.isActive == true){
                    return addUserOrder.save().then((orderPlaced, error) => {
                        if(error){
                            return false
                        } else {
                            return true
                        }
                    })
                    //return products
                }
        })
    } else {
        return "Admins are not allowed to place order"
    }
}


