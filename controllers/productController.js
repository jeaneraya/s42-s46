const Product = require('../models/Product');
const User = require('../models/User');

//Create a new Product
module.exports.addProduct = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return "Not an Admin"
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            return newProduct.save().then((course, error) => {
                if(error) {
                    return false
                } else {
                    return "Product Added Successfully"
                }
            })
        }
        
    });    
}


//Show All Active Products
module.exports.showActiveProducts = () => {

	return Product.find({isActive: true}).then(result => {

		return result
	})
};


//RETRIEVE SPECIFIC PRODUCT
module.exports.getProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then(result => {

        return result
    })

};


//UPDATE PRODUCT BY ADMIN ONLY
module.exports.updateProduct = (data, reqBody) => {

  if (data.isAdmin === true) {

      let updatedProduct = {

        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price

    }

      return Product.findByIdAndUpdate(data.productId, updatedProduct).then((product, error) => {

              if(error) {
                
                  return false
              }  else {

                  return true
              }
      })
         
  } else {

      return false
  } 

 }

 

 //ARCHIVE PRODUCT BY ADMIN ONLY
module.exports.archiveProduct = (data) => {

    return Product.findById(data.productId).then((result, error) => {

        if(data.isAdmin === true) {

            result.isActive = false;

            return result.save().then((product, error) => {

                if(error) {

                    return false;

                } else {

                    return true;
                }
            })

        } else {

            return false
        }

    })
}