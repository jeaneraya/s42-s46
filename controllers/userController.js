const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require('../models/Product');
const Order = require('../models/Order');


//User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10) 
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false

		} else {
			return true
		}

	})
};

//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}

			} else {
				return false
			}
		}

	})
};

//Set user as admin by admin only
module.exports.updateUserStatus = (userData,reqParams) => {
	if (userData.isAdmin == true) {
		return User.findById(reqParams.userId).then(user => {
			user.isAdmin = true
			return user.save().then((userIsAdmin, error) => {
				if(error){ return false }
					else { return true }
			})
		})
	} else {
		return "Not an Admin"
	}
}

// Retrieve User Details
module.exports.getUser = (reqParams) => {

	return User.findById(reqParams.userId).then((user, error) => {
		if(error){
			return false
		} else {
			return user
		}
	})

};

/*// Retrieve Auth User's Orders
module.exports.getOrdersFromAuthUser = (userData) => {
	return User.find(userData.userId).then(user => {
		if (user.isAdmin == false) {
			return Order.find(userData.userId).then((orders, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})
		} else {
			return false
		}
	})
	
}*/





