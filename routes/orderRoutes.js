const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth');

router.post('/:userId', auth.verify, (req, res) => {
	let userData = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	orderController.addUserOrder(req.body,userData).then(resultFromController => res.send(resultFromController))
	
})

module.exports = router;